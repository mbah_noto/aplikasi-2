var nama_aplikasi = "DASHBOARD RISET";

function ganti_judul(text){
    document.title = nama_aplikasi + " - " + text;
}

function tab_input(){
    ganti_judul("Input Data");
	$('#page-content').empty();
	$('#page-content').load('input.html');
}

function tab_simulasi(){
    ganti_judul("Simulasi");
    $('#page-content').empty();
    $('#page-content').load('simulasi.html');
}

function show_gauge(id, value, min, max, title){
	$('#' + id).empty();
	var g = new JustGage({
        id: id,
        value: value,
        min: min,
        max: max,
        title: title
    });
}